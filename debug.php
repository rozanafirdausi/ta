<?php
require_once __DIR__ . '/vendor/autoload.php';
require 'stopwords_removal.php';

use \NlpTools\Tokenizers\WhitespaceTokenizer;
use \NlpTools\Stemmers\PorterStemmer;

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "jurnall";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

function clean($string) {
	$string = str_replace(' ', ' ', $string);
	return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

$sql = "SELECT judul, abstrak FROM jurnal";
$result = $conn->query($sql);

$tok = new WhitespaceTokenizer();
$stem = new PorterStemmer();
$stemmerFactory = new \Sastrawi\Stemmer\StemmerFactory();
$stemmer  = $stemmerFactory->createStemmer();

if ($result->num_rows > 0) {
    $document = [];
    $totalDocumentOfTerm = [];
    $idx = 0;
    while($row = $result->fetch_assoc()) {
        $ld = new Text_LanguageDetect();
        $string =$row["judul"];
        $string = strtolower($string);
        $language = $ld->detectSimple($string);
        $listToken = $tok->tokenize($string);
        $listToken = clean($listToken);

        if($language!="english")
		{
            $listToken = removeCommonWordsIndonesian($listToken);
            $str = implode(',',$listToken);
            $listToken = $stemmer->stem($str);
            $listToken = $tok->tokenize($listToken);
		}
		else
		{
            $listToken = removeCommonWordsEnglish($listToken);
            $listToken = $stem->stemAll($listToken);
            $listToken = preg_replace("/[^a-zA-Z 0-9]+/", "", $listToken);
            $listToken = array_values(array_filter($listToken)); //menghilangkan array yang null setelah stopword removal
        }
        // print_r($listToken);
        $countToken = count($listToken);
        // echo ($countToken);
        $dictOfTerm = [];
        for($token=0; $token < $countToken; $token++ )
        {
            if(!array_key_exists($listToken[$token], $dictOfTerm))
            {
                $dictOfTerm[$listToken[$token]] =1;
                if(!array_key_exists($listToken[$token], $totalDocumentOfTerm))
                {
                    $totalDocumentOfTerm[$listToken[$token]] = 1;
                }
                else
                {
                    $totalDocumentOfTerm[$listToken[$token]] += 1;
                }
            }
            else
            {
                $dictOfTerm[$listToken[$token]] +=1;
            }
        }
        $document = array_merge_recursive($document,$dictOfTerm);
        $idx+=1;
        // print_r($document);
        
    }
    // print_r($dictOfTerm);
    $termFrequencyOfDocument = $document;
    $idx =0;
    
    echo $result->num_rows;
    for($terms=0 ; $terms < $result->num_rows; $terms++)
    {
        print_r($terms);
        $totalTerm =0;
        // for ($term=0; $term < count($terms) ; $term++)
        // {
        //     $totalTerm += $terms[$term];
        // }
        // for ($term=0; $term < count($terms) ; $term++)
        // {
        //     $termFrequencyOfDocument[$idx][$term] = floatval($document[$idx][$term]) / floatval($totalTerm);
        // }
        // print_r($termFrequencyOfDocument[$idx]);
        $idx +=1;
    }
}
else {
    echo "0 results";
}
$conn->close();

?>