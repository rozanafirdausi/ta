<?php
require_once __DIR__ . '/vendor/autoload.php';
require 'stopwords_removal.php';

use \NlpTools\Tokenizers\WhitespaceTokenizer;
use \NlpTools\Stemmers\PorterStemmer;

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "jurnall";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

function clean($string) {
	$string = str_replace(' ', ' ', $string);
	return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

$sql = "SELECT judul, abstrak FROM jurnal";
$result = $conn->query($sql);

$tok = new WhitespaceTokenizer();
$stem = new PorterStemmer();
$stemmerFactory = new \Sastrawi\Stemmer\StemmerFactory();
$stemmer  = $stemmerFactory->createStemmer();

function array_avg($array, $round=1){
    $num = count($array);
    return array_map(
        function($val) use ($num,$round){
            return array('count'=>$val,'avg'=>round($val/$num, $round));
        },
        array_count_values($array));
}

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $ld = new Text_LanguageDetect();
        $string =$row["judul"];
        $string = strtolower($string);
        $language = $ld->detectSimple($string);
        $string = $tok->tokenize($string);
        $string = clean($string);

        if($language!="english")
		{
            $string = removeCommonWordsIndonesian($string);
            $str = implode(',',$string);
            $string = $stemmer->stem($str);
            $string = $tok->tokenize($string);
            $termFrequency = array_avg($string);
            echo '<pre>'.print_r($termFrequency).'</pre>';
            // $termFrequency = array_count_values($string);
            // print_r($termFrequency);
		}
		else
		{
            $string = removeCommonWordsEnglish($string);
            $string = $stem->stemAll($string);
            $string = array_map('trim',$string); //ga ngefek
            // $termFrequency = array_count_values($string);
            $termFrequency = array_avg($string);
            echo '<pre>'.print_r($termFrequency,1).'</pre>';
			// print_r($termFrequency);
        }
        echo "<br> Bahasa: ". $language."<br>";
    }
    // echo "<br>";
}
else {
    echo "0 results";
}
$conn->close();

?>