import mysql.connector
import math
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory
 
factoryStopwordRemover = StopWordRemoverFactory()
stopword = factoryStopwordRemover.create_stop_word_remover()

factory = StemmerFactory()
stemmer = factory.create_stemmer()

class DBConnector:
    def __init__(self, user, passw, hostIp, dbname):        
        self.connection = mysql.connector.connect(user=user, password=passw,
                                      host=hostIp,
                                      database=dbname)
        self.cursor = self.connection.cursor(dictionary=True)

    def execute(self, queryString, params=()):
        self.cursor.execute(queryString, params)
        return self.cursor.fetchall()
DB = DBConnector('root', '', '127.0.0.1', 'jurnall')

statement = "SELECT * FROM jurnal"
result = DB.execute(statement)
document = []
totalDocumentOfTerm = {}
listKeluhan = []

idx = 0
for row in result:
    keluhan = row['judul'].encode('utf-8')
#     print keluhan
    listKeluhan.append(keluhan)
#     print listKeluhan
    stemmedKeluhan = stemmer.stem(keluhan)
    stemmedKeluhan = stopword.remove(stemmedKeluhan)
#     print stemmedKeluhan
    listToken = stemmedKeluhan.split()
#     print listToken
    
    dictOfTerm = {}
    for token in listToken:
        token = token.lower()
        token = token.strip(',.@?!_-"()')
#         print token
        if token not in dictOfTerm:
            dictOfTerm[token] = 1
#             print dictOfTerm
            if token not in totalDocumentOfTerm :
                totalDocumentOfTerm[token] = 1
#                 print totalDocumentOfTerm
            else :
                totalDocumentOfTerm[token] += 1
        else:
            dictOfTerm[token] += 1
#         print totalDocumentOfTerm 
    document.append(dictOfTerm)
    idx+=1
#     print document

# #normalisasi term dari setiap document
# #menghitung TF (term frequency)

# print document
termFrequencyOfDocument = document
# print termFrequencyOfDocument
# print "total = " + str(len(document))

idx=0
for terms in document:
#     print terms
    totalTerm = 0
    for term in terms:
        totalTerm += terms[term]
#         print term
    for term in terms:
        termFrequencyOfDocument[idx][term] = float(document[idx][term]) / float(totalTerm)
#     print termFrequencyOfDocument[idx]
    idx+=1
# menghitung inverse document frequency (IDF)

inverseDocumentFrequencyOfDocument = totalDocumentOfTerm
# print totalDocumentOfTerm
for term in inverseDocumentFrequencyOfDocument:
#     print term
    inverseDocumentFrequencyOfDocument[term] = 1 + math.log(float(len(document)) / float(totalDocumentOfTerm[term]))
    print inverseDocumentFrequencyOfDocument

# print inverseDocumentFrequencyOfDocument
#menghitung TF-IDF

tfIdf = document

idx=0
for terms in document:
    totalTerm = 0
    for term in terms:
        totalTerm += terms[term]
    for term in terms:
        TF = termFrequencyOfDocument[idx][term]
        IDF = inverseDocumentFrequencyOfDocument[term]
        tfIdf[idx][term] = float(TF) * float(IDF)
    print tfIdf[idx]
    idx+=1

def generateCombinedTermList(idx1, idx2):
    combinedTerm = []
    for term in termFrequencyOfDocument[idx1]:
        if term not in combinedTerm:
            combinedTerm.append(term)
    for term in termFrequencyOfDocument[idx2]:
        if term not in combinedTerm:
            combinedTerm.append(term)
    return combinedTerm

# mencoba gabung term doc 1 dan 2

generateCombinedTermList(0,1)

def generateDotProduct(idx1, idx2):
    termList = generateCombinedTermList(idx1, idx2)
    total = 0
    for term in termList:
        document1 = 0
        document2 = 0
        
        if term in tfIdf[idx1]:
            document1 = tfIdf[idx1][term]
        if term in tfIdf[idx2]:
            document2 = tfIdf[idx2][term]
        
        total += float(document1) * float(document2)
    
    return total

def generateLenght(idx1, idx2):
    termList = generateCombinedTermList(idx1, idx2)
    total1 = 0
    total2 = 0
    
    for term in termList:
        if term in tfIdf[idx1]:
            total1 += tfIdf[idx1][term] * tfIdf[idx1][term]
        if term in tfIdf[idx2]:
            total2 += tfIdf[idx2][term] * tfIdf[idx2][term]
    return math.sqrt(total1) * math.sqrt(total2)

class SimiliarityDocument:
    value = 0.0
    idxA = -1
    idxB = -1
    
    def __init__(self, value, idxA, idxB):
        self.value = value
        self.idxA = idxA
        self.idxB = idxB

listOfSimiliarity = []
for i in range(0, len(document)):
    for j in range(i+1, len(document)):
        if i == j :
            continue
            
        similiarity = float(generateDotProduct(i,j))/float(generateLenght(i,j))
        similiarityDocument = SimiliarityDocument(similiarity, i, j)
        listOfSimiliarity.append(similiarityDocument)

listOfSimiliarity.sort(key=lambda x: x.value, reverse=True)

for i in range(0, 10):
    similiarity = listOfSimiliarity[i]
    print "Similiarity : %f" % similiarity.value
    print "1 ==> %s" % listKeluhan[similiarity.idxA]
    print "2 ==> %s\n" % listKeluhan[similiarity.idxB]