<?php
require_once __DIR__ . '/vendor/autoload.php';
require 'stopwords_removal.php';

use \NlpTools\Tokenizers\WhitespaceTokenizer;
use \NlpTools\Stemmers\PorterStemmer;

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "jurnall";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

function clean($string) {
	$string = str_replace(' ', ' ', $string);
	return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

$sql = "SELECT judul, abstrak FROM jurnal";
$result = $conn->query($sql);

$tok = new WhitespaceTokenizer();
$stem = new PorterStemmer();
$stemmerFactory = new \Sastrawi\Stemmer\StemmerFactory();
$stemmer  = $stemmerFactory->createStemmer();

if ($result->num_rows > 0) {
    $totalDocumentOfTerm = [];

    $count = 0;

    while($row = $result->fetch_assoc()) {
        $ld = new Text_LanguageDetect();
        $string =$row["judul"];
        $totalDocument = $result->num_rows;
        // $document = $string;
        $string = strtolower($string);
        $language = $ld->detectSimple($string);
        $string = $tok->tokenize($string);
        $listToken = $string;
        // print_r($listToken);
        $string = clean($string);
        
        if($language!="english")
		{
            $string = removeCommonWordsIndonesian($string);
            $str = implode(',',$string);
            $string = $stemmer->stem($str);
            $string = $tok->tokenize($string);
            $num = count($string);
            $termFrequency = array_map(
                function($val) use ($num){
                    return floatval($val)/floatval($num);
                },
                array_count_values($string));
		}
		else
		{
            $string = removeCommonWordsEnglish($string);
            $string = $stem->stemAll($string);
            $string = preg_replace("/[^a-zA-Z 0-9]+/", "", $string);
            $string = array_values(array_filter($string)); //menghilangkan array yang null setelah stopword removal
            $num = count($string);
            $termFrequency = array_map(
                function($val) use ($num){
                    return floatval($val)/floatval($num);
                },
                array_count_values($string));
        }
        echo "<br>";
        //mencari IDF
        $arrlength = count($string);
        $varrr = [];
        $dictOfTerm =[];
        for($x = 0; $x < $arrlength; $x++) {
            // print_r($termFrequency[$string[$x]]);
            if(!array_key_exists($string[$x], $dictOfTerm)){
                $dictOfTerm[$string[$x]] = 1;
                if(!array_key_exists($string[$x], $totalDocumentOfTerm))
                {
                    $totalDocumentOfTerm[$string[$x]] =1;
                }
                else
                {
                    $totalDocumentOfTerm[$string[$x]] +=1;
                }
                
            }
            else{
                $dictOfTerm[$string[$x]] += 1;
            }
            $idf[$string[$x]] = log(floatval($totalDocument)/floatval($totalDocumentOfTerm[$string[$x]]));
            $log[$string[$x]] = floatval($totalDocument)/floatval($totalDocumentOfTerm[$string[$x]]);
            // $tfIdf[$string[$x]] = floatval($idf[$string[$x]]) * floatval($termFrequency[$string[$x]]);
            // print_r($termFrequency[$string[$x]]);
        }
        $tfs[$count] = $termFrequency;
        $count++;
    }
    // echo $count;
    echo $string[0];
    $token =0;
    // print_r($idf['sistem']);
    echo "<br>";
    // print_r($tfs[1]['sistem']);
    echo "<br>";
    $echu = floatval($idf['sistem'])*floatval($tfs[1]['sistem']);
    
    print_r($echu);
    // foreach($tfs as $tf)
    // {
    //     if(array_key_exists($tf[$token],$idf[$token]))
    //     {
    //         $tfIdf = floatval($tf[$token])*floatval($idf[$token]);
    //     }
    //     $token++;
    // }
    // print_r($tfIdf);
}
else {
    echo "0 results";
}
$conn->close();

?>