-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 19, 2018 at 11:47 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ojan`
--

-- --------------------------------------------------------

--
-- Table structure for table `jurnal`
--

CREATE TABLE `jurnal` (
  `id` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `jenis` varchar(100) NOT NULL,
  `judul` varchar(300) NOT NULL,
  `abstrak` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurnal`
--

INSERT INTO `jurnal` (`id`, `id_item`, `jenis`, `judul`, `abstrak`) VALUES
(1, 4, 'JL', 'Wave Speed Estimation using Video Coastal', ''),
(2, 5, 'JL', 'Analysis of Wave Celerity from Coastal Video Images using Cross-Correlation Approach', ''),
(3, 6, 'JL', 'Data-Model Assimilation Techniique to Estimate Nearshore Bethymety', ''),
(4, 7, 'JL', 'Nearshore Batymetry Estimation Using Video Coastal Monitoring System', ''),
(5, 8, 'SM', 'The Application of Vedeo Imagery System to Study Nerashore Batymetry', ''),
(6, 9, 'SM', 'The Influence of Raft Placement Height Variation of Double Seaweed Cultivation in Reducing The Wave', ''),
(7, 195, 'JL', 'Gabor-based Face Recognition With Illumination Variation Using Subspace-Linear Discriminant Analysis', 'Although Face recognition has been an active research topic in the past few decades due to its potential applications. Accurate face recognition is still a difficult task, especially in the case that illumination are unconstrained. This paper presents an efficient method for the recognition of faces with different illumination by using Gabor features, which are extracted by using log-Gabor filters of six orientations and four scales. By Using sliding window algorithm,  these features are extracted at image block-regions. Extracted features are passed to the Principal Component Analysis (PCA) and then to Linear Discriminant Analysis (LDA). For development and testing we used facial images from the Yale-B  databases. The proposed method achieved 86 ? 100  rank 1 recognition rate.'),
(8, 195, 'JL', 'Gabor-based Face Recognition With Illumination Variation Using Subspace-Linear Discriminant Analysis', 'Although Face recognition has been an active research topic in the past few decades due to its potential applications. Accurate face recognition is still a difficult task, especially in the case that illumination are unconstrained. This paper presents an efficient method for the recognition of faces with different illumination by using Gabor features, which are extracted by using log-Gabor filters of six orientations and four scales. By Using sliding window algorithm,  these features are extracted at image block-regions. Extracted features are passed to the Principal Component Analysis (PCA) and then to Linear Discriminant Analysis (LDA). For development and testing we used facial images from the Yale-B  databases. The proposed method achieved 86 ? 100  rank 1 recognition rate.'),
(9, 195, 'JL', 'Gabor-based Face Recognition With Illumination Variation Using Subspace-Linear Discriminant Analysis', 'Although Face recognition has been an active research topic in the past few decades due to its potential applications. Accurate face recognition is still a difficult task, especially in the case that illumination are unconstrained. This paper presents an efficient method for the recognition of faces with different illumination by using Gabor features, which are extracted by using log-Gabor filters of six orientations and four scales. By Using sliding window algorithm,  these features are extracted at image block-regions. Extracted features are passed to the Principal Component Analysis (PCA) and then to Linear Discriminant Analysis (LDA). For development and testing we used facial images from the Yale-B  databases. The proposed method achieved 86 ? 100  rank 1 recognition rate.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jurnal`
--
ALTER TABLE `jurnal`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jurnal`
--
ALTER TABLE `jurnal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
