<?php
function removeCommonWordsEnglish($input){
	$commonWords = array('a','able','about','above','abroad','according','accordingly','across','actually','adj','after','afterwards','again','against','ago','ahead','ain\'t','all','allow','allows','almost','alone','along','alongside','already','also','although','always','am','amid','amidst','among','amongst','an','and','another','any','anybody','anyhow','anyone','anything','anyway','anyways','anywhere','apart','appear','appreciate','appropriate','are','aren\'t','around','as','a\'s','aside','ask','asking','associated','at','available','away','awfully','b','back','backward','backwards','be','became','because','become','becomes','becoming','been','before','beforehand','begin','behind','being','believe','below','beside','besides','best','better','between','beyond','both','brief','but','by','c','came','can','cannot','cant','can\'t','caption','cause','causes','certain','certainly','changes','clearly','c\'mon','co','co.','com','come','comes','concerning','consequently','consider','considering','contain','containing','contains','corresponding','could','couldn\'t','course','c\'s','currently','d','dare','daren\'t','definitely','described','despite','did','didn\'t','different','directly','do','does','doesn\'t','doing','done','don\'t','down','downwards','during','e','each','edu','eg','eight','eighty','either','else','elsewhere','end','ending','enough','entirely','especially','et','etc','even','ever','evermore','every','everybody','everyone','everything','everywhere','ex','exactly','example','except','f','fairly','far','farther','few','fewer','fifth','first','five','followed','following','follows','for','forever','former','formerly','forth','forward','found','four','from','further','furthermore','g','get','gets','getting','given','gives','go','goes','going','gone','got','gotten','greetings','h','had','hadn\'t','half','happens','hardly','has','hasn\'t','have','haven\'t','having','he','he\'d','he\'ll','hello','help','hence','her','here','hereafter','hereby','herein','here\'s','hereupon','hers','herself','he\'s','hi','him','himself','his','hither','hopefully','how','howbeit','however','hundred','i','i\'d','ie','if','ignored','i\'ll','i\'m','immediate','in','inasmuch','inc','inc.','indeed','indicate','indicated','indicates','inner','inside','insofar','instead','into','inward','is','isn\'t','it','it\'d','it\'ll','its','it\'s','itself','i\'ve','j','just','k','keep','keeps','kept','know','known','knows','l','last','lately','later','latter','latterly','least','less','lest','let','let\'s','like','liked','likely','likewise','little','look','looking','looks','low','lower','ltd','m','made','mainly','make','makes','many','may','maybe','mayn\'t','me','mean','meantime','meanwhile','merely','might','mightn\'t','mine','minus','miss','more','moreover','most','mostly','mr','mrs','much','must','mustn\'t','my','myself','n','name','namely','nd','near','nearly','necessary','need','needn\'t','needs','neither','never','neverf','neverless','nevertheless','new','next','nine','ninety','no','nobody','non','none','nonetheless','noone','no-one','nor','normally','not','nothing','notwithstanding','novel','now','nowhere','o','obviously','of','off','often','oh','ok','okay','old','on','once','one','ones','one\'s','only','onto','opposite','or','other','others','otherwise','ought','oughtn\'t','our','ours','ourselves','out','outside','over','overall','own','p','particular','particularly','past','per','perhaps','placed','please','plus','possible','presumably','probably','provided','provides','q','que','quite','qv','r','rather','rd','re','really','reasonably','recent','recently','regarding','regardless','regards','relatively','respectively','right','round','s','said','same','saw','say','saying','says','second','secondly','see','seeing','seem','seemed','seeming','seems','seen','self','selves','sensible','sent','serious','seriously','seven','several','shall','shan\'t','she','she\'d','she\'ll','she\'s','should','shouldn\'t','since','six','so','some','somebody','someday','somehow','someone','something','sometime','sometimes','somewhat','somewhere','soon','sorry','specified','specify','specifying','still','sub','such','sup','sure','t','take','taken','taking','tell','tends','th','than','thank','thanks','thanx','that','that\'ll','thats','that\'s','that\'ve','the','their','theirs','them','themselves','then','thence','there','thereafter','thereby','there\'d','therefore','therein','there\'ll','there\'re','theres','there\'s','thereupon','there\'ve','these','they','they\'d','they\'ll','they\'re','they\'ve','thing','things','think','third','thirty','this','thorough','thoroughly','those','though','three','through','throughout','thru','thus','till','to','together','too','took','toward','towards','tried','tries','truly','try','trying','t\'s','twice','two','u','un','under','underneath','undoing','unfortunately','unless','unlike','unlikely','until','unto','up','upon','upwards','us','use','used','useful','uses','using','usually','v','value','various','versus','very','via','viz','vs','w','want','wants','was','wasn\'t','way','we','we\'d','welcome','well','we\'ll','went','were','we\'re','weren\'t','we\'ve','what','whatever','what\'ll','what\'s','what\'ve','when','whence','whenever','where','whereafter','whereas','whereby','wherein','where\'s','whereupon','wherever','whether','which','whichever','while','whilst','whither','who','who\'d','whoever','whole','who\'ll','whom','whomever','who\'s','whose','why','will','willing','wish','with','within','without','wonder','won\'t','would','wouldn\'t','x','y','yes','yet','you','you\'d','you\'ll','your','you\'re','yours','yourself','yourselves','you\'ve','z','zero');
 
	return preg_replace('/\b('.implode('|',$commonWords).')\b/','',$input);
}

function removeCommonWordsIndonesian($input){
	$commonWords = array(
        'ada',
        'adalah',
        'adanya',
        'adapun',
        'agak',
        'agaknya',
        'agar',
        'akan',
        'akankah',
        'akhir',
        'akhiri',
        'akhirnya',
        'aku',
        'akulah',
        'amat',
        'amatlah',
        'anda',
        'andalah',
        'antar',
        'antara',
        'antaranya',
        'apa',
        'apaan',
        'apabila',
        'apakah',
        'apalagi',
        'apatah',
        'artinya',
        'asal',
        'asalkan',
        'atas',
        'atau',
        'ataukah',
        'ataupun',
        'awal',
        'awalnya',
        'bagai',
        'bagaikan',
        'bagaimana',
        'bagaimanakah',
        'bagaimanapun',
        'bagi',
        'bagian',
        'bahkan',
        'bahwa',
        'bahwasanya',
        'baik',
        'bakal',
        'bakalan',
        'balik',
        'banyak',
        'bapak',
        'baru',
        'bawah',
        'beberapa',
        'begini',
        'beginian',
        'beginikah',
        'beginilah',
        'begitu',
        'begitukah',
        'begitulah',
        'begitupun',
        'bekerja',
        'belakang',
        'belakangan',
        'belum',
        'belumlah',
        'benar',
        'benarkah',
        'benarlah',
        'berada',
        'berakhir',
        'berakhirlah',
        'berakhirnya',
        'berapa',
        'berapakah',
        'berapalah',
        'berapapun',
        'berarti',
        'berawal',
        'berbagai',
        'berdatangan',
        'beri',
        'berikan',
        'berikut',
        'berikutnya',
        'berjumlah',
        'berkali-kali',
        'berkata',
        'berkehendak',
        'berkeinginan',
        'berkenaan',
        'berlainan',
        'berlalu',
        'berlangsung',
        'berlebihan',
        'bermacam',
        'bermacam-macam',
        'bermaksud',
        'bermula',
        'bersama',
        'bersama-sama',
        'bersiap',
        'bersiap-siap',
        'bertanya',
        'bertanya-tanya',
        'berturut',
        'berturut-turut',
        'bertutur',
        'berujar',
        'berupa',
        'besar',
        'betul',
        'betulkah',
        'biasa',
        'biasanya',
        'bila',
        'bilakah',
        'bisa',
        'bisakah',
        'boleh',
        'bolehkah',
        'bolehlah',
        'buat',
        'bukan',
        'bukankah',
        'bukanlah',
        'bukannya',
        'bulan',
        'bung',
        'cara',
        'caranya',
        'cukup',
        'cukupkah',
        'cukuplah',
        'cuma',
        'dahulu',
        'dalam',
        'dan',
        'dapat',
        'dari',
        'daripada',
        'datang',
        'dekat',
        'demi',
        'demikian',
        'demikianlah',
        'dengan',
        'depan',
        'di',
        'dia',
        'diakhiri',
        'diakhirinya',
        'dialah',
        'diantara',
        'diantaranya',
        'diberi',
        'diberikan',
        'diberikannya',
        'dibuat',
        'dibuatnya',
        'didapat',
        'didatangkan',
        'digunakan',
        'diibaratkan',
        'diibaratkannya',
        'diingat',
        'diingatkan',
        'diinginkan',
        'dijawab',
        'dijelaskan',
        'dijelaskannya',
        'dikarenakan',
        'dikatakan',
        'dikatakannya',
        'dikerjakan',
        'diketahui',
        'diketahuinya',
        'dikira',
        'dilakukan',
        'dilalui',
        'dilihat',
        'dimaksud',
        'dimaksudkan',
        'dimaksudkannya',
        'dimaksudnya',
        'diminta',
        'dimintai',
        'dimisalkan',
        'dimulai',
        'dimulailah',
        'dimulainya',
        'dimungkinkan',
        'dini',
        'dipastikan',
        'diperbuat',
        'diperbuatnya',
        'dipergunakan',
        'diperkirakan',
        'diperlihatkan',
        'diperlukan',
        'diperlukannya',
        'dipersoalkan',
        'dipertanyakan',
        'dipunyai',
        'diri',
        'dirinya',
        'disampaikan',
        'disebut',
        'disebutkan',
        'disebutkannya',
        'disini',
        'disinilah',
        'ditambahkan',
        'ditandaskan',
        'ditanya',
        'ditanyai',
        'ditanyakan',
        'ditegaskan',
        'ditujukan',
        'ditunjuk',
        'ditunjuki',
        'ditunjukkan',
        'ditunjukkannya',
        'ditunjuknya',
        'dituturkan',
        'dituturkannya',
        'diucapkan',
        'diucapkannya',
        'diungkapkan',
        'dong',
        'dua',
        'dulu',
        'empat',
        'enggak',
        'enggaknya',
        'entah',
        'entahlah',
        'guna',
        'gunakan',
        'hal',
        'hampir',
        'hanya',
        'hanyalah',
        'hari',
        'harus',
        'haruslah',
        'harusnya',
        'hendak',
        'hendaklah',
        'hendaknya',
        'hingga',
        'ia',
        'ialah',
        'ibarat',
        'ibaratkan',
        'ibaratnya',
        'ibu',
        'ikut',
        'ingat',
        'ingat-ingat',
        'ingin',
        'inginkah',
        'inginkan',
        'ini',
        'inikah',
        'inilah',
        'itu',
        'itukah',
        'itulah',
        'jadi',
        'jadilah',
        'jadinya',
        'jangan',
        'jangankan',
        'janganlah',
        'jauh',
        'jawab',
        'jawaban',
        'jawabnya',
        'jelas',
        'jelaskan',
        'jelaslah',
        'jelasnya',
        'jika',
        'jikalau',
        'juga',
        'jumlah',
        'jumlahnya',
        'justru',
        'kala',
        'kalau',
        'kalaulah',
        'kalaupun',
        'kalian',
        'kami',
        'kamilah',
        'kamu',
        'kamulah',
        'kan',
        'kapan',
        'kapankah',
        'kapanpun',
        'karena',
        'karenanya',
        'kasus',
        'kata',
        'katakan',
        'katakanlah',
        'katanya',
        'ke',
        'keadaan',
        'kebetulan',
        'kecil',
        'kedua',
        'keduanya',
        'keinginan',
        'kelamaan',
        'kelihatan',
        'kelihatannya',
        'kelima',
        'keluar',
        'kembali',
        'kemudian',
        'kemungkinan',
        'kemungkinannya',
        'kenapa',
        'kepada',
        'kepadanya',
        'kesampaian',
        'keseluruhan',
        'keseluruhannya',
        'keterlaluan',
        'ketika',
        'khususnya',
        'kini',
        'kinilah',
        'kira',
        'kira-kira',
        'kiranya',
        'kita',
        'kitalah',
        'kok',
        'kurang',
        'lagi',
        'lagian',
        'lah',
        'lain',
        'lainnya',
        'lalu',
        'lama',
        'lamanya',
        'lanjut',
        'lanjutnya',
        'lebih',
        'lewat',
        'lima',
        'luar',
        'macam',
        'maka',
        'makanya',
        'makin',
        'malah',
        'malahan',
        'mampu',
        'mampukah',
        'mana',
        'manakala',
        'manalagi',
        'masa',
        'masalah',
        'masalahnya',
        'masih',
        'masihkah',
        'masing',
        'masing-masing',
        'mau',
        'maupun',
        'melainkan',
        'melakukan',
        'melalui',
        'melihat',
        'melihatnya',
        'memang',
        'memastikan',
        'memberi',
        'memberikan',
        'membuat',
        'memerlukan',
        'memihak',
        'meminta',
        'memintakan',
        'memisalkan',
        'memperbuat',
        'mempergunakan',
        'memperkirakan',
        'memperlihatkan',
        'mempersiapkan',
        'mempersoalkan',
        'mempertanyakan',
        'mempunyai',
        'memulai',
        'memungkinkan',
        'menaiki',
        'menambahkan',
        'menandaskan',
        'menanti',
        'menanti-nanti',
        'menantikan',
        'menanya',
        'menanyai',
        'menanyakan',
        'mendapat',
        'mendapatkan',
        'mendatang',
        'mendatangi',
        'mendatangkan',
        'menegaskan',
        'mengakhiri',
        'mengapa',
        'mengatakan',
        'mengatakannya',
        'mengenai',
        'mengerjakan',
        'mengetahui',
        'menggunakan',
        'menghendaki',
        'mengibaratkan',
        'mengibaratkannya',
        'mengingat',
        'mengingatkan',
        'menginginkan',
        'mengira',
        'mengucapkan',
        'mengucapkannya',
        'mengungkapkan',
        'menjadi',
        'menjawab',
        'menjelaskan',
        'menuju',
        'menunjuk',
        'menunjuki',
        'menunjukkan',
        'menunjuknya',
        'menurut',
        'menuturkan',
        'menyampaikan',
        'menyangkut',
        'menyatakan',
        'menyebutkan',
        'menyeluruh',
        'menyiapkan',
        'merasa',
        'mereka',
        'merekalah',
        'merupakan',
        'meski',
        'meskipun',
        'meyakini',
        'meyakinkan',
        'minta',
        'mirip',
        'misal',
        'misalkan',
        'misalnya',
        'mula',
        'mulai',
        'mulailah',
        'mulanya',
        'mungkin',
        'mungkinkah',
        'nah',
        'naik',
        'namun',
        'nanti',
        'nantinya',
        'nyaris',
        'nyatanya',
        'oleh',
        'olehnya',
        'pada',
        'padahal',
        'padanya',
        'pak',
        'paling',
        'panjang',
        'pantas',
        'para',
        'pasti',
        'pastilah',
        'penting',
        'pentingnya',
        'per',
        'percuma',
        'perlu',
        'perlukah',
        'perlunya',
        'pernah',
        'persoalan',
        'pertama',
        'pertama-tama',
        'pertanyaan',
        'pertanyakan',
        'pihak',
        'pihaknya',
        'pukul',
        'pula',
        'pun',
        'punya',
        'rasa',
        'rasanya',
        'rata',
        'rupanya',
        'saat',
        'saatnya',
        'saja',
        'sajalah',
        'saling',
        'sama',
        'sama-sama',
        'sambil',
        'sampai',
        'sampai-sampai',
        'sampaikan',
        'sana',
        'sangat',
        'sangatlah',
        'satu',
        'saya',
        'sayalah',
        'se',
        'sebab',
        'sebabnya',
        'sebagai',
        'sebagaimana',
        'sebagainya',
        'sebagian',
        'sebaik',
        'sebaik-baiknya',
        'sebaiknya',
        'sebaliknya',
        'sebanyak',
        'sebegini',
        'sebegitu',
        'sebelum',
        'sebelumnya',
        'sebenarnya',
        'seberapa',
        'sebesar',
        'sebetulnya',
        'sebisanya',
        'sebuah',
        'sebut',
        'sebutlah',
        'sebutnya',
        'secara',
        'secukupnya',
        'sedang',
        'sedangkan',
        'sedemikian',
        'sedikit',
        'sedikitnya',
        'seenaknya',
        'segala',
        'segalanya',
        'segera',
        'seharusnya',
        'sehingga',
        'seingat',
        'sejak',
        'sejauh',
        'sejenak',
        'sejumlah',
        'sekadar',
        'sekadarnya',
        'sekali',
        'sekali-kali',
        'sekalian',
        'sekaligus',
        'sekalipun',
        'sekarang',
        'sekecil',
        'seketika',
        'sekiranya',
        'sekitar',
        'sekitarnya',
        'sekurang-kurangnya',
        'sekurangnya',
        'sela',
        'selagi',
        'selain',
        'selaku',
        'selalu',
        'selama',
        'selama-lamanya',
        'selamanya',
        'selanjutnya',
        'seluruh',
        'seluruhnya',
        'semacam',
        'semakin',
        'semampu',
        'semampunya',
        'semasa',
        'semasih',
        'semata',
        'semata-mata',
        'semaunya',
        'sementara',
        'semisal',
        'semisalnya',
        'sempat',
        'semua',
        'semuanya',
        'semula',
        'sendiri',
        'sendirian',
        'sendirinya',
        'seolah',
        'seolah-olah',
        'seorang',
        'sepanjang',
        'sepantasnya',
        'sepantasnyalah',
        'seperlunya',
        'seperti',
        'sepertinya',
        'sepihak',
        'sering',
        'seringnya',
        'serta',
        'serupa',
        'sesaat',
        'sesama',
        'sesampai',
        'sesegera',
        'sesekali',
        'seseorang',
        'sesuatu',
        'sesuatunya',
        'sesudah',
        'sesudahnya',
        'setelah',
        'setempat',
        'setengah',
        'seterusnya',
        'setiap',
        'setiba',
        'setibanya',
        'setidak-tidaknya',
        'setidaknya',
        'setinggi',
        'seusai',
        'sewaktu',
        'siap',
        'siapa',
        'siapakah',
        'siapapun',
        'sini',
        'sinilah',
        'soal',
        'soalnya',
        'suatu',
        'sudah',
        'sudahkah',
        'sudahlah',
        'supaya',
        'tadi',
        'tadinya',
        'tahu',
        'tahun',
        'tak',
        'tambah',
        'tambahnya',
        'tampak',
        'tampaknya',
        'tandas',
        'tandasnya',
        'tanpa',
        'tanya',
        'tanyakan',
        'tanyanya',
        'tapi',
        'tegas',
        'tegasnya',
        'telah',
        'tempat',
        'tengah',
        'tentang',
        'tentu',
        'tentulah',
        'tentunya',
        'tepat',
        'terakhir',
        'terasa',
        'terbanyak',
        'terdahulu',
        'terdapat',
        'terdiri',
        'terhadap',
        'terhadapnya',
        'teringat',
        'teringat-ingat',
        'terjadi',
        'terjadilah',
        'terjadinya',
        'terkira',
        'terlalu',
        'terlebih',
        'terlihat',
        'termasuk',
        'ternyata',
        'tersampaikan',
        'tersebut',
        'tersebutlah',
        'tertentu',
        'tertuju',
        'terus',
        'terutama',
        'tetap',
        'tetapi',
        'tiap',
        'tiba',
        'tiba-tiba',
        'tidak',
        'tidakkah',
        'tidaklah',
        'tiga',
        'tinggi',
        'toh',
        'tunjuk',
        'turut',
        'tutur',
        'tuturnya',
        'ucap',
        'ucapnya',
        'ujar',
        'ujarnya',
        'umum',
        'umumnya',
        'ungkap',
        'ungkapnya',
        'untuk',
        'usah',
        'usai',
        'waduh',
        'wah',
        'wahai',
        'waktu',
        'waktunya',
        'walau',
        'walaupun',
        'wong',
        'yaitu',
        'yakin',
        'yakni',
        'yang'
    );
 
	return preg_replace('/\b('.implode('|',$commonWords).')\b/','',$input);
}
?>