<?php
require_once __DIR__ . '/vendor/autoload.php';
require 'stopwords_removal.php';

use \NlpTools\Tokenizers\WhitespaceTokenizer;
use \NlpTools\Stemmers\PorterStemmer;

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "jurnall";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

function clean($string) {
	$string = str_replace(' ', ' ', $string);
	return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

$sql = "SELECT judul, abstrak FROM jurnal";
$result = $conn->query($sql);

$tok = new WhitespaceTokenizer();
$stem = new PorterStemmer();
$stemmerFactory = new \Sastrawi\Stemmer\StemmerFactory();
$stemmer  = $stemmerFactory->createStemmer();

function array_avg($array){
    $num = count($array);
    return array_map(
        function($val) use ($num){
            return floatval($val)/floatval($num);
        },
        array_count_values($array));
}
if ($result->num_rows > 0) {
    $totalDocumentOfTerm = [];
    while($row = $result->fetch_assoc()) {
        $ld = new Text_LanguageDetect();
        $string =$row["judul"];
        $totalDocument = $result->num_rows;
        $document = $string;
        $string = strtolower($string);
        $language = $ld->detectSimple($string);
        $string = $tok->tokenize($string);
        $listToken = $string;
        // print_r($listToken);
        $string = clean($string);

        if($language!="english")
		{
            $string = removeCommonWordsIndonesian($string);
            $str = implode(',',$string);
            $string = $stemmer->stem($str);
            $string = $tok->tokenize($string);
            $termFrequency = array_avg($string);
		}
		else
		{
            $string = removeCommonWordsEnglish($string);
            $string = $stem->stemAll($string);
            $string = preg_replace("/[^a-zA-Z 0-9]+/", "", $string);
            $string = array_values(array_filter($string)); //menghilangkan array yang null setelah stopword removal
            $termFrequency = array_avg($string);
            
        }
        echo "<br>";
        //mencari IDF
        $arrlength = count($string);
        $varrr = [];
        $dictOfTerm =[];
        for($x = 0; $x < $arrlength; $x++) {
            if(!array_key_exists($string[$x], $dictOfTerm)){
                $dictOfTerm[$string[$x]] = 1;
                if(!array_key_exists($string[$x], $totalDocumentOfTerm))
                {
                    $totalDocumentOfTerm[$string[$x]] =1;
                }
                else
                {
                    $totalDocumentOfTerm[$string[$x]] +=1;
                }
                
            }
            else{
                $dictOfTerm[$string[$x]] += 1;
            }
            $idf[$string[$x]] = log(floatval($totalDocument)/floatval($totalDocumentOfTerm[$string[$x]]));
            $log[$string[$x]] = floatval($totalDocument)/floatval($totalDocumentOfTerm[$string[$x]]);
            // $test = floatval(log(15/3));
            // $tfIdf[$string[$x]] = floatval($idf[$string[$x]]) * floatval($termFrequency);
        
        }
        print_r($tfIdf);
        // print_r($termFrequency[$string[$x]]);
        
        
        
        // echo "<br> Bahasa: ". $language."<br>";
    }
}
else {
    echo "0 results";
}
$conn->close();

?>